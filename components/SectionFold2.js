
const SectionFold2 = () => {
    return(
        <section className="testimonial-banner" id="download">
        <div className="container px-5">
          <h2 className="text-center text-white font-alt mb-4">Get the app now!</h2>
          <div className="d-flex flex-column flex-lg-row align-items-center justify-content-center">
            <a className="me-lg-3 mb-4 mb-lg-0" href="https://play.google.com/" target= "_blank"><img className="app-badge" src="assets/img/google-play-badge.svg"  width="165px" alt="Google Play Store"/></a>
            <a href="https://www.apple.com/ph/app-store/" target="_blank"><img className="app-badge" src="assets/img/app-store-badge.svg" width="165px" alt="Apple App Store"/></a>
          </div>
        </div>
      </section>
    )
}

export default SectionFold2