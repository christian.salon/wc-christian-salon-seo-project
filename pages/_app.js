import '../styles/pages.css'
import '../styles/globals.css'
import Head from 'next/head'

function MyApp({ Component, pageProps }) {
  
  return (
    <>
    <Head>
      <title>Rally</title>
      <meta name="title" content="Rally"/>
      <meta name="description" content="The power of an all-in-one Financial Managment tool, in the palm of your hands."/>

      <meta property="og:type" content="website"/>
      <meta property="og:url" content="https://wc-christian-salon-seo-project.vercel.app/"/>
      <meta property="og:title" content="Rally"/>
      <meta property="og:description" content="The power of an all-in-one Financial Managment tool, in the palm of your hands."/>
      <meta property="og:image" content="https://ik.imagekit.io/rally/banner-image_UEOT7ihBe.png?ik-sdk-version=javascript-1.4.3&updatedAt=1655027336331"></meta>

      <meta property="twitter:card" content="summary_large_image"/>
      <meta property="twitter:url" content="https://wc-christian-salon-seo-project.vercel.app/"/>
      <meta property="twitter:title" content="Rally"/>
      <meta property="twitter:description" content="The power of an all-in-one Financial Managment tool, in the palm of your hands."/>
      <meta property="twitter:image" content="https://ik.imagekit.io/rally/banner-image_UEOT7ihBe.png?ik-sdk-version=javascript-1.4.3&updatedAt=1655027336331"></meta>
    </Head>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" 
          rel="stylesheet" 
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" 
          crossOrigin="anonymous">
    </link>
    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" 
            integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" 
            crossOrigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" 
            integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy" 
            crossOrigin="anonymous">
    </script>
    <Component {...pageProps} />
    </>
    )
}

export default MyApp
